package com.example.s528737.emeralertsys;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;


public class MainActivity extends Activity {
      
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main);
        Button bt1=(Button)findViewById(R.id.button5);
        bt1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent it=new Intent(MainActivity.this,Login.class);
                startActivity(it);
            }
        });
 
 
 

        ImageButton btnClick = findViewById(R.id.buttonClick);
         
        btnClick.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,SecondActivity.class);

                startActivity(intent);
            }
        });
        ImageButton btnClick2 = findViewById(R.id.buttonClick2);

        btnClick2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, third.class);

                startActivity(intent);
            }
        });

    }

     




        }
